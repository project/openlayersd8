OpenLayersD8

ONLY WORKS WITH WKT
Geofield works with WKT
for following kinds of geometry:
Polygon => POLYGON ((30 10, 40 40, 20 40, 10 20, 30 10))
Point => POINT (30 10)
Line => LINESTRING(-12 40,-52 36,8 46,17 12)

usable for:
output of geoms
input of geoms
create maps based on wms
create map and show it as a block
add special node to map


Next Steps:
make the code ready for a real project

make views working
add vector tiles like wfs
add content type as layer to map
add views as layer to map


function to add a toolbar
- getfeatureinfo
- measure

Have fun with it and feel free to test it
best regards Tim aka AndyLicht