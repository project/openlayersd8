<?php

namespace Drupal\openlayers_geolocation_geometry\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
//use Drupal\geofield\GeoPHP\GeoPHPInterface;

/**
 * Plugin implementation of the 'geofield_OpenLayersInputWidget' widget.
 *
 * @FieldWidget(
 *   id = "openlayers_geolocation_geometry_inputwidget",
 *   label = @Translation("OpenLayers"),
 *   field_types = {
 *     "geolocation_geometry_geometry",
 *     "geolocation_geometry_geometrycollection",
 *     "geolocation_geometry_point",
 *     "geolocation_geometry_linestring",
 *     "geolocation_geometry_polygon",
 *     "geolocation_geometry_multipoint",
 *     "geolocation_geometry_multilinestring",
 *     "geolocation_geometry_multipolygon",
 *   }
 * )
 */
class OpenLayersInputWidget extends WidgetBase implements ContainerFactoryPluginInterface {
  
  
  /**
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;
      
  /**
   * @var \Drupal\geofield\GeoPHP\GeoPHPInterface
   */
  protected $geoPHP;
  
  /**
   * @var Drupal\Core\Field\FieldDefinitionInterface::getType()
   */
//  protected $fieldType;
  
  /**
  * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
  * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
  * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
  */
  
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->entityTypeManager = $entity_type_manager;
    //$this->geoPHP = $geoPHP;
    $this->fieldType = $field_definition->getType();
  }
  
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager'),
      //$container->get('geofield.geophp')
    );
  }

  /**
   * Gives a visual Option to save Geometries.
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    
    $geom_type = explode("_", $this->fieldType)[2];
    $info = $this->buildTypeInfo($geom_type);
    $drawFunction = FALSE;

    $mapid = Html::getUniqueId('openlayers_map');
    $map = openlayers_get_map($this->getSetting('openlayers_map'));
    
    foreach ($map['navbar']['controls'] as $index => $control) {
      if ($control['machine'] === 'Draw') {
        $drawFunction = TRUE;
      }
    }  
    
    if(!$drawFunction) {
      $control = [
        'name' => 'Draw',
        'machine' => 'Draw',
        'type' => 'custom',
        'namespace' => 'customcontrol',
        'factory' => 'openlayers',
        'parameter' => [
          'target' => '{uuid}-wktbox',
          'type' => $info['type'],
        ],
        'tooltip' => $info['tooltip'],
        'icon' => $info['icon']
      ];
      array_push($map['navbar']['controls'], $control);
    }
    
    $element['map'] = openlayers_render_map($mapid, $map, NULL, TRUE);
    
    if($this->getSetting('openlayers_showbox')) {
      $valueType = 'hidden';
    } 
    else {
      $valueType = 'textarea';
    }
    
    $element['wkt'] = [
      '#type' => $valueType,
      '#title' => $this->t('Geom as WKT(') . $element['#title'] . ')' ,
      '#default_value' => $items[$delta]->wkt ?: NULL,
      '#attributes' => ['id' => $mapid.'-wktbox'],
    ];
    return $element;
  }
  
  private function buildTypeInfo ($geomType) {
    
    $info = [];
    
    if ($geomType != 'geometry' && $geomType != 'geometrycollection') {
      
      if(strpos($geomType, 'multi') !== false) {
        $geomType = str_replace('multi', '', $geomType);
        $geomType = ucfirst($geomType);
        $geomType =  'multi' . $geomType;
      }
      
      if(strpos($geomType, 'string') !== false) {
        $geomType = str_replace('string', '', $geomType);
        $geomType =  $geomType . 'String';
      }
      
      $geomType =  ucfirst($geomType);
      
      $info['type'] = $geomType;
      $info['icon'] = strtolower($geomType) . '.svg';
      $info['tooltip'] = 'Draw ' .  $geomType;
    } 
    elseif ($geomType === 'geometry') {
      $info['type'] = 'Point|MultPoint|LineString|MultiLineString|Polygon|MultiPolygon';
      $info['tooltip'] = 'Draw Point|Draw MultPoint|Draw Line|Draw MultiLine|Draw Polygon|Draw MultiPolygon';
      $info['icon'] = 'point.svg|multipoint.svg|line.svg|multiline.svg|polygon.svg|multipolygon.svg';
    }
    return $info;
  }
  
  /**
  * {@inheritdoc}
  */
  public static function defaultSettings() {
    return [
      'openlayers_map' => 'none selected',
      'openlayers_showbox' => false,
    ] + parent::defaultSettings();
  }
  
  /**
  * {@inheritdoc}
  */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    
    $elements = parent::settingsForm($form, $form_state);

    $openlayers_map_options = [];
    foreach (openlayers_get_map() as $key => $map) {
      $openlayers_map_options[$key] = $this->t($map['label']);
    }
    
    $elements['openlayers_map'] = [
      '#title' => $this->t('OpenLayers Map'),
      '#type' => 'select',
      '#options' => $openlayers_map_options,
      '#default_value' => $this->getSetting('openlayers_map'),
      '#required' => TRUE,
    ];
    
    $elements['openlayers_showbox'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide input value box'),
      '#attributes' => ['id' => ['openlayers-map-showbox']],
      '#default_value' => $this->getSetting('openlayers_showbox'),
    ];
    return $elements;
  }
  
  
  /**
  * {@inheritdoc}
  */
  public function settingsSummary() {
    $map = openlayers_get_map($this->getSetting('openlayers_map'));
    //$map = openlayers_get_map('dasdassda');
    $summary = [];
    if(isset($map['label'])) {
      $summary[] = $this->t('OpenLayers MAP: @map', ['@map' => $this->t($map['label'])]); 
    } else {
      $summary[] = $this->t('none map is configured or configured map is still missing');
    }
    return $summary;
  }
}
