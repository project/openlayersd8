<?php

namespace Drupal\openlayers_views\Plugin\views\style;

use Drupal\core\form\FormStateInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;
use Drupal\Component\Utility\Html;
/**
 * Style plugin to render a list of years and months
 * in reverse chronological order linked to content.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "OpenLayersViews",
 *   title = @Translation("OpenLayers Views"),
 *   help = @Translation("Render a list of Content as map."),
 *   theme = "openlayers_map_views",
 *   display_types = { "normal" }
 * )
 */
class OpenlayersViews extends StylePluginBase {
  /**
   * Whether the display allows the use of a pager or not.
   *
   * @var bool
   */
  protected $usesPager = FALSE;

  /**
   * Whether the display allows the use of a 'more' link or not.
   *
   * @var bool
   */
  protected $usesMore = FALSE;

  /**
   * Whether the display allows area plugins.
   *
   * @var bool
   */
  protected $usesAreas = FALSE;

  /**
   * Does the style plugin support grouping of rows.
   */
  protected $usesGrouping = FALSE;

  /**
   * Does the style plugin for itself support to add fields to it's output.
   */
  protected $usesFields = TRUE;
  
  protected $usesRowPlugin = TRUE;
  
  
  /**
   * {@inheritdoc}
   */
  public function evenEmpty() {
    // Render map even if there is no data.
    return TRUE;
  }
  
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['openlayers_map'] = array('default' => 'none selected');
    $options['openlayers_map_view_field'] = array('default' => 'none selected');
    $options['activatehover'] = array('default' => FALSE);
    $options['hoverelement'] = array('default' => '');
    $options['idelement'] = array('default' => '');
    $options['idattr'] = array('default' => '');
    $options['hovercolor'] = array('default' => '');
    $options['hovereffect'] = array('default' => 'background-color');
    return $options;
  }
  
   /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $openlayers_map_options = [];
    foreach (openlayers_get_map() as $key => $map) {
      $openlayers_map_options[$key] = $this->t($map['label']);
    }
    
    $form['openlayers_map'] = [
      '#title' => $this->t('OpenLayers Map'),
      '#type' => 'select',
      '#options' => $openlayers_map_options,
      '#default_value' => $this->options['openlayers_map'],
      '#required' => TRUE,
    ];
    $view = entity_load('view', $this->view->storage->id());
    $fields = $view->get('display')['default']['display_options']['fields'];
    
    $geofields = array();
    foreach($fields as $field_name => $field) {
      if($field['type'] === 'geofield_default' || $field['type'] === 'geofield' ) {
        $geofields[$field_name] = $field_name;
      }
    }
    $form['openlayers_map_view_field'] = [
      '#title' => $this->t('Field to show'),
      '#type' => 'select',
      '#options' => $geofields,
      '#default_value' => $this->options['openlayers_map_view_field'],
      '#required' => TRUE,
    ];
    
    $form['activatehover'] = [
      '#title' => $this->t('Activate Hovereffect'),
      '#type' => 'checkbox',
      '#default_value' => $this->options['activatehover'],
    ];
    
    $form['hoverelement'] = [
      '#title' => $this->t('Element for the Hover effect'),
      '#type' => 'textfield',
      '#default_value' => $this->options['hoverelement'],      
    ];
    
    $form['idelement'] = [
      '#title' => $this->t('Element of the ID'),
      '#description' => $this->t('Leave empty if it is the same like the hover element'),
      '#type' => 'textfield',
      '#default_value' => $this->options['idelement'],      
    ];
    
    $form['idattr'] = [
      '#title' => $this->t('Attribute which contains the ID'),
      '#type' => 'textfield',
      '#default_value' => $this->options['idattr'],   
      '#description' => $this->t('Leave empty if the id is the value'),
    ];
    
    $form['hovercolor'] = [
      '#title' => $this->t('Color of the hover effect'),
      '#type' => 'color',
      '#default_value' => $this->options['hovercolor'],      
    ];
    
    $hovereffectoptions = [
      'color' => 'color',
      'background-color' => 'background-color',
    ];
    
    $form['hovereffect'] = [
      '#title' => $this->t('Color of the hover effect'),
      '#type' => 'select',
      '#default_value' => $this->options['hovereffect'], 
      '#options' => $hovereffectoptions,
    ];
  }
  
  /**
   * {@inheritdoc}
   */
  public function render() {
    $map = openlayers_get_map($this->options['openlayers_map']);
    $mapid = Html::getUniqueId('openlayers_map');    
    $features = [];
    if($this->options['openlayers_map_view_field'] === NULL) {
      $element = null;
      return $element;
    } else {
      //each row of the view
      foreach ($this->view->result as $row_index => $row) {
        //check if geofield is null, only not null values are used
        if( NULL !== $this->getFieldValue($row_index, $this->options['openlayers_map_view_field'])) {
          foreach ( $this->getFieldValue($row_index, $this->options['openlayers_map_view_field']) as $geom_id => $geom ) {
            $id = $row_index . $geom_id; 
            foreach ($this->view->field as $field_id => $field ) {
              $features[$id][$field_id] = $field->getValue($row);
            }
            $features[$id]['geom'] = $geom;
            $features[$id]['group'] = $features[$id]['nid'];
            unset($features[$id][$this->options['openlayers_map_view_field']]);
          }
        }
      }
    }
    $libs = [];
    $settings = [];
    if($this->options['activatehover'] == 1) {
      array_push($libs, 'openlayers_views/openlayersviews');
      $settings['openlayersviews'] = [];
      $settings['openlayersviews'] = $this->options;
      $settings['openlayersviews']['mapid'] = $mapid;
    };
    $element = openlayers_render_map($mapid, $map, $features, FALSE, $libs, $settings);
    return $element;
  }  
}