(function ($) {
  'use strict';
  console.log("JS Drupal OpenLayers - Views");
  Drupal.behaviors.openlayersviews = {
    attach: function (context, settings) {
      $.each(settings.openlayers, function (m, data) {
        //check if div exists
        if(document.getElementById(data.map_div_id)) {
          $('#' + data.map_div_id, context).once('setLink').each(function () {
            //Hover effect if cursor is over the map  
            var color_ = null;
            var key = null;
            var selected = null;
            var map = document.getElementById(settings.openlayersviews.mapid).data;
            var hoverInteraction = new ol.interaction.Select({
              condition: ol.events.condition.pointerMove,
            });
            
            hoverInteraction.on('select', function(evt){
              if(evt.selected.length > 0 && selected !== evt.selected) {
                console.log("element auf der karte selektiert");
                selected = evt.selected;
                if (settings.openlayersviews.idelement !== '') {
                  if(settings.openlayersviews.idattr !== '') {
                    $(key).closest(settings.openlayersviews.hoverelement).css(settings.openlayersviews.hovereffect, color_);
                    key = settings.openlayersviews.idelement + "[" + settings.openlayersviews.idattr + "*='" + evt.selected[0].get('group') + "']";
                    color_ = $(key).closest(settings.openlayersviews.hoverelement).css(settings.openlayersviews.hovereffect);
                    $(key).closest(settings.openlayersviews.hoverelement).css(settings.openlayersviews.hovereffect, settings.openlayersviews.hovercolor);
                  } else {
                    $(key).closest(settings.openlayersviews.hoverelement).css(settings.openlayersviews.hovereffect, color_);
                    key = settings.openlayersviews.idelement + ":contains(\"" + evt.selected[0].get('group') + "\")";
                    color_ = $(key).closest(settings.openlayersviews.hoverelement).css(settings.openlayersviews.hovereffect);
                    $(key).closest(settings.openlayersviews.hoverelement).css(settings.openlayersviews.hovereffect, settings.openlayersviews.hovercolor);  
                  }
                } else {
                  if(settings.openlayersviews.idattr !== '') {  
                    $(key).css(settings.openlayersviews.hovereffect, color_);
                    key = settings.openlayersviews.idelement + "[" + settings.openlayersviews.idattr + "*='" + evt.selected[0].get('group') + "']";
                    color_ = $(key).css(settings.openlayersviews.hovereffect);
                    $(key).css(settings.openlayersviews.hovereffect, settings.openlayersviews.hovercolor);  
                  } else {
                    $(key).closest(settings.openlayersviews.hoverelement).css(settings.openlayersviews.hovereffect, color_);
                    key = settings.openlayersviews.idelement + ":contains(\"" + evt.selected[0].get('group') + "\")";
                    color_ = $(key).closest(settings.openlayersviews.hoverelement).css(settings.openlayersviews.hovereffect);
                    $(key).closest(settings.openlayersviews.hoverelement).css(settings.openlayersviews.hovereffect, settings.openlayersviews.hovercolor);
                  }
                }
              } else {
                if (settings.openlayersviews.idelement !== '') {
                  $(key).closest(settings.openlayersviews.hoverelement).css(settings.openlayersviews.hovereffect, color_);
                } else {
                  $(key).css(settings.openlayersviews.hovereffect, color_); 
                }
              }
            });   
            
            map.getViewport().addEventListener('mouseout', function(evt){
              hoverInteraction.getFeatures().clear();
              if (settings.openlayersviews.idelement !== '') {
                $(key).closest(settings.openlayersviews.hoverelement).css(settings.openlayersviews.hovereffect, color_);
              } else {
                $(key).css(settings.openlayersviews.hovereffect, color_); 
              }
              color_ = null;
              key = null;
              selected = null;
            }, false);
            map.addInteraction(hoverInteraction);
            var selectInteraction = new ol.interaction.Select();
            map.addInteraction(selectInteraction);
            //Hover effect if cursor is over a list item  
            
            $( settings.openlayersviews.hoverelement )
              .mouseenter(function() {
                var a_href = null;
                if (settings.openlayersviews.idelement !== '') {
                  if(settings.openlayersviews.idattr !== '') {
                    a_href = $(this).find(settings.openlayersviews.idelement).attr(settings.openlayersviews.idattr);
                  } else {
                    a_href = $(this).find(settings.openlayersviews.idelement).text(); 
                  }
                } else {
                  if(settings.openlayersviews.idattr !== '') {
                    a_href = $(this).attr(settings.openlayersviews.idattr);
                  } else {
                    a_href = $(this).text();
                  }
                }
                var id = a_href.replace(/[^0-9]/gi, '');
                id = parseInt(id);
                var featExtent = null;
                map.getLayers().forEach(function(layer) {
                  if (layer.get('id') === 'fO') {
                    var features = [];
                    layer.getSource().forEachFeature(function(feature){
                      if(feature.get('group') == id){
                        if(featExtent === null) {
                          featExtent = feature.getGeometry().getExtent();
                          selectInteraction.getFeatures().push(feature);
                        } else {
                          ol.extent.extend(featExtent, feature.getGeometry().getExtent());
                        }
                        features.push(feature);
                      }
                    })
                  }
                });
                if(featExtent) {
                  color_ = $(this).css(settings.openlayersviews.hovereffect);
                  $(this).css(settings.openlayersviews.hovereffect, settings.openlayersviews.hovercolor);
                  map.getView().fit(featExtent, map.getSize());
                }
              })
              .mouseleave(function() {
                $(this).css(settings.openlayersviews.hovereffect, color_);
                color_ = null;
                selectInteraction.getFeatures().clear();
              });
          });
        }
      });
    }
  };
})(jQuery);