<?php

namespace Drupal\openlayers_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Provides a Map as a Block.
 *
 * @Block(
 *   id = "openlayers_block",
 *   admin_label = @Translation("OpenLayers Map"),
 *   category = @Translation("OpenLayers Map"),
 * )
 */
class OpenLayers_Block extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;
  
  /**
  * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
  * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
  * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
  * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
  */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
  }
  
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  } 
  
  /**
   * {@inheritdoc}
   */
  public function build() {
    $mapid = Html::getUniqueId('openlayers_map_block');
    $build = array();
    $element['#markup'] = $this->configuration['label'];
    $map = openlayers_get_map($this->configuration['openlayers_block_map']);
    $element = openlayers_render_map($mapid, $map, NULL, FALSE);
    $build['map'] = $element;
    return $build;
  }
  
  /*
   * Block Settings.
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();
    $openlayers_map_options = [];
    
    foreach (openlayers_get_map() as $key => $map) {
      $openlayers_map_options[$key] = $this->t($map['label']);
    }
    
    $form['openlayers_block_map'] = [
      '#title' => $this->t('OpenLayers Map'),
      '#type' => 'select',
      '#options' => $openlayers_map_options,
      '#default_value' => isset($config['openlayers_block_map']) ? $config['openlayers_block_map'] : '',
      '#required' => TRUE,
    ];
    return $form;
  }
  
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['openlayers_block_map'] = $values['openlayers_block_map'];
  }
}
