<?php

namespace Drupal\openlayers_geofield\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\geofield\GeoPHP\GeoPHPInterface;

/**
 * Plugin implementation of the 'geofield_OpenLayersInputWidget' widget.
 *
 * @FieldWidget(
 *   id = "geofield_OpenLayersInputWidget",
 *   label = @Translation("OpenLayers"),
 *   field_types = {
 *     "geofield"
 *   }
 * )
 */
class OpenLayersInputWidget extends WidgetBase implements ContainerFactoryPluginInterface {
  
  
  /**
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;
      
  /**
   * @var \Drupal\geofield\GeoPHP\GeoPHPInterface
   */
  protected $geoPHP;
  
  /**
  * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
  * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
  * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
  */
  
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, EntityTypeManagerInterface $entity_type_manager, GeoPHPInterface $geoPHP) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->entityTypeManager = $entity_type_manager;
    $this->geoPHP = $geoPHP;
  }
  
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager'),
      $container->get('geofield.geophp')
    );
  }

  /**
   * Gives a visual Option to save Geometries.
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $drawFunction = FALSE;

    $mapid = Html::getUniqueId('openlayers_map');
    $map = openlayers_get_map($this->getSetting('openlayers_map'));
    
    foreach ($map['navbar']['controls'] as $index => $control) {
      if ($control['machine'] === 'Draw') {
        $drawFunction = TRUE;
      }
    }  
    
    if(!$drawFunction) {
      $control = [
        'name' => 'Draw',
        'machine' => 'Draw',
        'type' => 'custom',
        'namespace' => 'customcontrol',
        'factory' => 'openlayers',
        'parameter' => [
          'target' => '{uuid}-wktbox',
          'type' => 'Point|LineString|Polygon',
        ],
        'tooltip' => 'Draw Point|Draw Line|Draw Polygon',
        'icon' => 'point.svg|line.svg|polygon.svg'
      ];
      array_push($map['navbar']['controls'], $control);
    }
    
    $element['map'] = openlayers_render_map($mapid, $map, NULL, TRUE);
    
    if($this->getSetting('openlayers_showbox')) {
      $type = 'hidden';
    } else {
      $type = 'textarea';
    }
    
    $element['value'] = [
      '#type' => $type,
      '#title' => $this->t('Geom as WKT'),
      '#default_value' => $items[$delta]->value ?: NULL,
      '#attributes' => ['id' => $mapid.'-wktbox'],
    ];
    return $element;
  }

  /**
  * {@inheritdoc}
  */
  public static function defaultSettings() {
    return [
      'openlayers_map' => 'none selected',
      'openlayers_showbox' => false,
    ] + parent::defaultSettings();
  }
  
  /**
  * {@inheritdoc}
  */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    
    $config = $this->fieldDefinition;
    
    $elements = parent::settingsForm($form, $form_state);

    $openlayers_map_options = [];
    foreach (openlayers_get_map() as $key => $map) {
      $openlayers_map_options[$key] = $this->t($map['label']);
    }
    
    $elements['openlayers_map'] = [
      '#title' => $this->t('OpenLayers Map'),
      '#type' => 'select',
      '#options' => $openlayers_map_options,
      '#default_value' => $this->getSetting('openlayers_map'),
      '#required' => TRUE,
    ];
    
    $elements['openlayers_showbox'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide input value box'),
      '#attributes' => ['id' => ['openlayers-map-showbox']],
      '#default_value' => $this->getSetting('openlayers_showbox'),
    ];
    return $elements;
  }
  
  
  /**
  * {@inheritdoc}
  */
  public function settingsSummary() {
    $map = openlayers_get_map($this->getSetting('openlayers_map'));
    //$map = openlayers_get_map('dasdassda');
    $summary = [];
    if(isset($map['label'])) {
      $summary[] = $this->t('OpenLayers MAP: @map', ['@map' => $this->t($map['label'])]); 
    } else {
      $summary[] = $this->t('none map is configured or configured map is still missing');
    }
    return $summary;
  }
  
  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $geophp = $this->geoPHP;
    foreach ($values as $delta => $value) {
      if ($geom = $geophp->load($value['value'])) {
        $values[$delta]['value'] = $geom->out('wkt');
      }
    }
    return $values;
  }
  
  /**
   * {@inheritdoc}
   * This function is called from parent::view().
   */
//  public function viewElements(FieldItemListInterface $items, $langcode) {
//    $settings = $this->getSettings();
//    $map = openlayers_get_map($settings['openlayers_map']);
//    $elements = [];
//    foreach ($items as $delta => $item) {
//      $features = openlayers_process_geofield($item->value);
//      $elements[$delta] = openlayers_render_map($map, $features);
//    }
//    return $elements;
//  }
}
