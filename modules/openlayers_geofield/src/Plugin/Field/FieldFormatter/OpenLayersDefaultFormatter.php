<?php

namespace Drupal\openlayers_geofield\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Plugin implementation of the 'openlayers_default' formatter.
 *
 * @FieldFormatter(
 *   id = "openlayers_formatter_default",
 *   label = @Translation("OpenLayers"),
 *   field_types = {
 *     "geofield"
 *   }
 * )
 */
class OpenLayersDefaultFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  
  /**
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;
      
  /**
  * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
  * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
  * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
  */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->entityManager = $entity_type_manager;
  }
  
  /**
   * {@inheritdoc}
   */  
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      // Add any services you want to inject here
      $container->get('entity.manager')
    );
  }
  
  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'openlayers_map' => null,
    ] + parent::defaultSettings();
  }

  /**
   * Formular bei der Auswahl der DisplaySettings.
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $openlayers_map_options = [];
    foreach (openlayers_get_map() as $key => $map) {
      $openlayers_map_options[$key] = $this->t($map['label']);
    }
    
    $elements['openlayers_map'] = [
      '#title' => $this->t('OpenLayers Map'),
      '#type' => 'select',
      '#options' => $openlayers_map_options,
      '#default_value' => $this->getSetting('openlayers_map'),
      '#required' => TRUE,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $map = openlayers_get_map($this->getSetting('openlayers_map'));
    $summary = [];
    if(isset($map['label'])) {
      $summary[] = $this->t('OpenLayers MAP: @map', ['@map' => $this->t($map['label'])]); 
    } else {
      $summary[] = $this->t('none map is configured or configured map is still missing');
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   *
   * This function is called from parent::view().
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $settings = $this->getSettings();
    $map = openlayers_get_map($settings['openlayers_map']);
    $mapid = Html::getUniqueId('openlayers_map');
    $elements = [];
    $features = [];
    foreach ($items as $delta => $item) {
      
      $feature = [];
      //$feature['geom'] = openlayers_process_geofield($item->value);
      $feature['geom'] = $item->value;
      array_push($features, $feature);
      $elements[$delta] = openlayers_render_map($mapid, $map, $features, FALSE);
    }
    return $elements;
  }

  /**
   * Validate Url method.
   *
   * @param array $element
   *   The element to validate.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The state of the form.
   */
  public function validateUrl($element, FormStateInterface $form_state) {
    if (!empty($element['#value']) && !UrlHelper::isValid($element['#value'])) {
      $form_state->setError($element, $this->t("Icon Url is not valid."));
    }
  }

}
