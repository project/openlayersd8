<?php

/**
 * @file
 * Contains \Drupal\openlayers\Form\MapCloneForm.
 */

namespace Drupal\openlayers\Form;

use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a form for deleting a content_entity_example entity.
 *
 * @ingroup openlayers
 */
class MapCloneForm extends ContentEntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to clone map %name?', array('%name' => $this->entity->label()));
  }

  /**
   * {@inheritdoc}
   *
   * If the delete command is canceled, return to the contact list.
   */
  public function getCancelUrl() {
    return new Url('entity.openlayers.map.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Clone');
  }

  /**
   * {@inheritdoc}
   *
   * Delete the entity and log the event. logger() replaces the watchdog.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $entity = $this->getEntity();
    $clone = $entity->createDuplicate();
    $clone->set('map_name', 'Clone: ' . $entity->get('map_name')->getValue()[0]['value']);
    $clone->save();
    $this->logger('map')->notice('cloned %title.',
      array(
        '%title' => $this->entity->label(),
      ));
    // Redirect to term list after delete.
    $form_state->setRedirect('entity.openlayers.map.collection');
  }
}
