<?php
/**
 * @file
 * Contains \Drupal\openlayers\Form\LayerForm.
 */

namespace Drupal\openlayers\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\openlayers\Controller\ExternalLayerTree;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for the content_entity_example entity edit forms.
 *
 * @ingroup content_entity_example
 */
class LayerForm extends ContentEntityForm {

   
    
  /**
   * {@inheritdoc}
   */
  private $sourcelayerrelation = [];
  
  /**
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;
  
  /**
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;
  
  /**
  * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
  * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
  * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
  * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
  */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityRepositoryInterface $entity_repository) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityRepository = $entity_repository;
  }
  
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity.repository')
    );
  }
  
  public function buildForm(array $form, FormStateInterface $form_state) {
    /*
     * kind of source => kind of layer
     * OSM, XYZ => tile
     * ImageWMS => image 
     * vector => view, node
     */
    
    $this->sourcelayerrelation['tile'][0] = 'osm';
    $this->sourcelayerrelation['tile'][1] = 'XYZ';
    $this->sourcelayerrelation['image'][0] = 'imagewms';
    $this->sourcelayerrelation['node'][0] = 'vector';
    $this->sourcelayerrelation['view'][0] = 'vector';
    
    $form['wrapper'] = array(
      '#type' => 'container',
      '#attributes' => array('id' => 'data-wrapper'),
    );
    /* @var $entity \Drupal\openlayers\Entity\OpenLayersLayer */
    $form['wrapper'][] = parent::buildForm($form, $form_state);
    $form['#attached'] = array(
      'library' => array('openlayers/drupal-LayerForm-addLayer'),
      'drupalSettings' => array(),
    );
    $form['wrapper'][0]['layer_type']['widget']['#ajax'] = [
      'callback' => '::rebuildLayerForm',
      'event' => 'change',
      'wrapper' => 'data-wrapper',
      'progress' => [
        'type' => 'throbber',
        'message' => $this->t('...'),
      ],
    ]; 
    
    /*
     * Check if Source is defined.
     */
    if (isset($form_state->getValue('layer_source_ref')[0]['target_id'])) {
      $tmp_sourceid = $form_state->getValue('layer_source_ref')[0]['target_id'];
      unset($form['wrapper'][0]['layer_source_ref']['widget']['#default_value']);
    } else {
      $tmp_sourceid = $form['wrapper'][0]['layer_source_ref']['widget']['#default_value'];
    }
    if(isset($form_state->getValue('layer_type')[0]['value']) && ($form['wrapper'][0]['layer_type']['widget']['#default_value'][0] !== $form_state->getValue('layer_type')[0]['value'])) {
      unset($form['wrapper'][0]['layer_source_ref']['widget']['#default_value']);
    }
        
    /*
     * Check if the wms is not changed, if changed clear the textarea.
     */
    if(is_array($form_state->getValue('layer_machinename'))) {
      $form_state->setValue('layer_machinename','');
      $form['wrapper'][0]['layer_machinename']['widget'][0]['value']['#default_value'] = 'none';
    }    
    
    if(!is_array($form_state->getValue('layer_type'))) {
      $layerType = $form['wrapper'][0]['layer_type']['widget']['#default_value'][0];
    } else {
      $layerType = $form_state->getValue('layer_type')[0]['value'];
    }
    switch ($layerType) {
      case 'tile':
        $form['wrapper'][0]['layer_source_ref']['widget']['#options'] = $this->buildSourcesforLayerType('tile');
        $form['wrapper'][0]['layer_source_ref']['#access'] = TRUE;
        $form['wrapper'][0]['layer_view_ref']['#access'] = FALSE;
        $form['wrapper'][0]['layer_node_ref']['#access'] = FALSE;
        $form['wrapper'][0]['layer_machinename']['#access'] = FALSE;
        break;
      case 'image':     
        $src_options = $this->buildSourcesforLayerType('image');
        $form['wrapper'][0]['layer_machinename']['#prefix'] = '<div id="layercontainer"><div class="column col-textarea" >';
        $form['wrapper'][0]['layer_source_ref']['widget']['#options'] = $src_options;
        $form['wrapper'][0]['layer_source_ref']['widget']['#ajax']= [
          'callback' => '::rebuildLayerForm',
          'event' => 'change',
          'wrapper' => 'data-wrapper',
          'progress' => [
            'type' => 'throbber',
            'message' => $this->t('...'),
          ],
        ];   
        
        if(isset($tmp_sourceid) && array_key_exists($tmp_sourceid, $src_options)) {
          $options = $this->buildLayerOptions($tmp_sourceid, $form['wrapper'][0]['layer_machinename']['widget'][0]['value']['#default_value']);
          $form['wrapper'][0]['layer_machinename']['#access'] = TRUE;
          $form['wrapper'][0]['layer_machinename']['#suffix'] = '</div><div class="column col-controls"><div id="openlayers_copyall" class="button button--primary button-ol-control" >Copy all</div><div id="openlayers_clearlayerfield" class="button button--primary button-ol-control" >Clear</div></div><div class="column col-layer" id="layeroptions"><div id="labellayeroptions">available Layer</div>' . $options . '</div></div>';
        } else {
          $form['wrapper'][0]['layer_machinename']['#access'] = FALSE;
        }
        $form['wrapper'][0]['layer_source_ref']['#access'] = TRUE;
        $form['wrapper'][0]['layer_view_ref']['#access'] = FALSE;
        $form['wrapper'][0]['layer_node_ref']['#access'] = FALSE;
        break;
      case 'node':
        $form['wrapper'][0]['layer_source_ref']['widget']['#options'] = $this->buildSourcesforLayerType('node');
        $form['wrapper'][0]['layer_source_ref']['#access'] = TRUE;
        $form['wrapper'][0]['layer_view_ref']['#access'] = FALSE;
        $form['wrapper'][0]['layer_node_ref']['#access'] = TRUE;
        $form['wrapper'][0]['layer_machinename']['#access'] = FALSE;
        break;
      case 'view':
        $form['wrapper'][0]['layer_source_ref']['widget']['#options'] = $this->buildSourcesforLayerType('view');
        $form['wrapper'][0]['layer_source_ref']['#access'] = TRUE;
        $form['wrapper'][0]['layer_view_ref']['widget']['#options'] = $this->buildViewsforLayerType();
        $form['wrapper'][0]['layer_view_ref']['#access'] = TRUE;
        $form['wrapper'][0]['layer_node_ref']['#access'] = FALSE;
        $form['wrapper'][0]['layer_machinename']['#access'] = FALSE;
        break;
    }
    return $form;
  }

  public function rebuildLayerForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild(true); 
    return $form['wrapper'];
  }
  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    // Redirect to term list after save.
    $form_state->setRedirect('entity.openlayers.layer.collection');
    $entity = $this->getEntity();
    $entity->save();
  }
  
  /*
   * build the form depend on the choosen layer type
   */
  private function buildSourcesforLayerType($type) {
    $options =[];
    $ids = [];
    foreach ($this->sourcelayerrelation[$type] as $_type) {
      $storage = $this->entityTypeManager->getStorage('openlayers_source');
      $_ids = $storage->getQuery()
        ->condition('source_type',$_type)
        ->execute();
      
      if (is_array($_ids)) {
        foreach ($_ids as $id) {
          array_push($ids, $id);
        }
      }
    }
    
    $sources = $this->entityTypeManager->getStorage('openlayers_source')->loadMultiple($ids);
    
    foreach ($sources as $source) { 
      $options[$source->id()] = $source->label();
    }
    
    return $options;
  }
  
  //for Views
  private function buildViewsforLayerType() {
    $options = [];
    
    $storage = $this->entityTypeManager->getStorage('view');
    $views = $storage->getQuery()->execute();
    
    foreach ($views as &$view) {
      $_view = $this->entityTypeManager->getStorage('view')->load($view);
      $fields = $_view->get('display')['default']['display_options']['fields'];
      
      if (is_array($fields)) {
        foreach($fields as $field) {
          if (in_array("type",array_keys($field))) {
            if($field['type'] === 'geofield_default' || $field['type'] === 'geofield' ) {
              $options[$_view->id()] = $_view->label();
              break;
            }
          }   
        }
      };
    }     
    return $options;    
  }
  
  private function buildLayerOptions($source_id, $layers) {
    $layerTree = new ExternalLayerTree($source_id, $layers);
    return $layerTree->asHTML();
  }
}
