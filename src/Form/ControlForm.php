<?php
/**
 * @file
 * Contains \Drupal\openlayers\Form\ControlForm.
 */

namespace Drupal\openlayers\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the content_entity_example entity edit forms.
 *
 * @ingroup content_entity_example
 */
class ControlForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    
    
    /* @var $entity \Drupal\openlayers\Entity\OpenLayersControl */
    $form = parent::buildForm($form, $form_state);

    //$form['icon']['widget'][0]['value']['#description'] = \Drupal\Core\Field\FieldFilteredMarkup::create(t('Select a icon from the list below'));
    $form['#attached'] = array(
      'library' => array('openlayers/drupal-ControlForm-addIcon'),
      'drupalSettings' => array(),
    );
    unset($form['icon']['widget'][0]['value']['#description']);
    if (isset($form['icon']['widget'][0]['value']['#default_value'])) {
      $options = $this->buildIconOptions($form['icon']['widget'][0]['value']['#default_value']);
    } else {
      $options = $this->buildIconOptions();
    }    
    $form['icon']['#prefix'] = '<div id="IconsSettings">';
    $form['icon']['#suffix'] = '<div id="availableIcons">' . $options . '</div></div>';
    
    return $form;
  }
  
  private function buildIconOptions($activeIcon = null) {
    $options = '';
    $path = drupal_get_path('module', 'openlayers') . '/img';
    $files = scandir($path, 1);
    foreach ($files as $value) {
      if($value != '.' && $value != '..') {
        if($activeIcon === $value) {
          $options = $options . '<img title="' . $value . '" class="icon current" src="../../../../../' . $path . "/" . $value . '" alt="' . $value . '">';
        } else {
          $options = $options . '<img title="' . $value . '" class="icon" src="../../../../../' . $path . "/" . $value . '" alt="' . $value . '">';
        }
      }
    }
    return $options;
  }
  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    // Redirect to term list after save.
    $form_state->setRedirect('entity.openlayers.control.collection');
    $entity = $this->getEntity();
    $entity->save();
  }
}
