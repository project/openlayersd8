<?php

namespace Drupal\openlayers\Controller;


class ExternalLayerTree {
  
  private $source_id;
  private $source_url;
  private $options;
  private $layers;
  
  public function __construct($source_id = null, $layers = null) {
    $this->source_id = $source_id;
    $this->source_url = $this->getURL();
    $this->setOptions();
    $this->layers = explode(",", $layers);
  }
  
  private function getURL() {
    $source = \Drupal::entityManager()->getStorage('openlayers_source')->load($this->source_id);
    return $source->get('source_url')->value;
  }
  
  public function getSourceURL() {
    return $this->source_url;
  }
  
  private function setOptions() {
    $set =\Drupal\Core\Site\Settings::get('http_client_config');
    $proxy = $set['proxy'];
    //Get GetCapabilies.
    try {
      $client = \Drupal::httpClient(['timeout' => 3.0]);
      $request = $client->get($this->source_url.'service=wms&request=getcapabilities', ['proxy' => $proxy, 'http_errors' => false]);
    
      //Make XML-DOcument.
      $xml = new \SimpleXMLElement($request->getBody());    
      $this->buildOptions($xml->xpath("/*/*[local-name()='Capability']/*[local-name()='Layer']"));
      return $this->options;
    } catch (\GuzzleHttp\Exception\ConnectException $e) {
      return $this->options['error'] = (string)$e->getMessage();
    } catch (Exception $e) {
      return $this->options['error'] = (string)$e->getMessage();
    }
  }
  
  private function buildOptions($items) {
    if(sizeof($items) > 0) {
      foreach($items as $layer) {
        if (sizeof($layer->Layer) > 0) {
          $this->buildOptions($layer->Layer);
        } else {
          $this->options[(string)$layer->Name] = (string)$layer->Title;
        };
      }
    }
  }
  
  public function asHTML(){
    $html = "";
    foreach ($this->options as $key => $value) {
      if(in_array($key, $this->layers)) {
        $html = $html . '<div class="wmslayer oltextboxcontent-disable" title="remove" layer="'. $key .'">' . $value .' ('. $key .')</div>' ;
      } else {
        $html = $html . '<div class="wmslayer oltextboxcontent-enable" title="add" layer="'. $key .'">' . $value .' ('. $key .')</div>' ;
      }
    }
    return $html;
  }
}