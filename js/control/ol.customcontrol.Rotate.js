ol.customcontrol.Rotate = function(opt_options, settings, mapid, olMap) {
  console.log("Rotate: " + mapid);
  var options = opt_options || {};

  var button = document.createElement('img');
  button.title = settings.tooltip;
  button.className = "ol-control-icon";
  var protocol = location.protocol;
  var slashes = protocol.concat("//");
  var host = slashes.concat(window.location.hostname);
  button.src = host + settings.path +"/img/" + settings.icon;
  

  var element = document.createElement('div');
  element.className = 'ol-unselectable ol-control';
  element.appendChild(button);
  var this_ = this;
  
  var count = 0;
  var clickCount = 0;
  
  var handleRotateNorth = function(evt) {  
    var targetElement = event.target || event.srcElement;       
    clickCount++;  
    if (clickCount === 1) {
      singleClickTimer = setTimeout(function() {
        clickCount = 0;            
      }, 200);
      var init = this_.getMap().getView().getRotation() / ((2 * Math.PI) / 360);
      var rot = ((2 * Math.PI) / 360) * (init + parseFloat(settings.parameter.angle));
      this_.getMap().getView().setRotation(rot); 
      targetElement.setAttribute('style','transform:rotate(' + (init + parseFloat(settings.parameter.angle)) + 'deg)');
    } else if (clickCount >= 2) {
      count++;
      clearTimeout(singleClickTimer);
      clickCount = 0;
      this_.getMap().getView().setRotation( 0 );
      targetElement.setAttribute('style','transform:rotate(0deg)');
    }
  };
  
  button.addEventListener('click', handleRotateNorth, false);

  ol.control.Control.call(this, {
    element: element,
    target: options.target
  });
};
ol.inherits(ol.customcontrol.Rotate, ol.control.Control);


