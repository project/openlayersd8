ol.customcontrol.ZoomOut = function(opt_options, settings, mapid, olMap) {
  console.log("ZoomOut: " + mapid);
  var options = opt_options || {};

  var button = document.createElement('img');
  button.title = settings.tooltip;
  button.className = "ol-control-icon";
  var protocol = location.protocol;
  var slashes = protocol.concat("//");
  var host = slashes.concat(window.location.hostname);
  button.src = host + settings.path +"/img/" + settings.icon;

  var element = document.createElement('div');
  element.className = 'ol-unselectable ol-control';
  element.appendChild(button);

  var this_ = this;
  var zoomOut = function() {
    this_.getMap().getView().setZoom(this_.getMap().getView().getZoom() - 0.5);  
  };

  button.addEventListener('click', zoomOut, false);
  button.addEventListener('touchstart', zoomOut, false);

  var element = document.createElement('div');
  element.className = 'ol-unselectable ol-control';
  element.appendChild(button);

  ol.control.Control.call(this, {
    element: element,
    target: options.target
  });
};
ol.inherits(ol.customcontrol.ZoomOut, ol.control.Control);


