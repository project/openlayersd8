ol.customcontrol.ZoomIn = function(opt_options, settings, mapid, olMap) {
  console.log("ZoomIn: " + mapid);
  var options = opt_options || {};

  var button = document.createElement('img');
  button.title = settings.tooltip;
  button.className = "ol-control-icon";
  var protocol = location.protocol;
  var slashes = protocol.concat("//");
  var host = slashes.concat(window.location.hostname);
  button.src = host + settings.path +"/img/" + settings.icon;
  
  var element = document.createElement('div');
  element.className = 'ol-unselectable ol-control';
  element.appendChild(button);

    var this_ = this;
    var zoomIn = function() {
      this_.getMap().getView().setZoom(this_.getMap().getView().getZoom() + 0.5);  
    };

    button.addEventListener('click', zoomIn, false);
    button.addEventListener('touchstart', zoomIn, false);

    ol.control.Control.call(this, {
      element: element,
      target: options.target
    });

};
ol.inherits(ol.customcontrol.ZoomIn, ol.control.Control);


