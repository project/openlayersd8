(function ($) {
  ol.customcontrol.Draw = function(opt_options, settings, mapid, olMap) {
    console.log('Draw: ' + mapid + ' Type: ' + settings.parameter.type);
    var options = opt_options || {};
    
    var types = settings.parameter.type.split("|");
    var tooltips = settings.tooltip.split("|");
    var icons = settings.icon.split("|");
    var groupelement = document.createElement('div');

    //OpenLayers
    var format = new ol.format.WKT();
    var geomType = null;
    var map = olMap;
    var draw_ = null;
    var activeElement = null;
    var inputSource = new ol.source.Vector({wrapX: false});
    var inputLayer = new ol.layer.Vector({
      source: inputSource,
      name: 'drawLayer'
    }); 
    var wkt = null;

    map.addLayer(inputLayer);  

    var drawFunction = function() {
      if(this === activeElement) {
        activeElement = this;
        this.classList.remove('activated');
        activeElement = null;
        removeDraw();
      } else {
        removeDraw();
        geomType = this.getAttribute('geometry');
        addDraw();
        var list = document.getElementsByClassName(mapid + " activated");
        for(var i = 0; i < list.length; i++) {
          list[i].classList.remove('activated');
        }
        activeElement = this;
        this.classList.add('activated');
      }
    }

    var removeDraw = function() {
      map.getInteractions().forEach(function (interaction) {
        if(interaction.get('name') === 'drupalDraw') {
          map.removeInteraction(interaction);
        }
      });
    }

    var addDraw = function() {
      draw_ = new ol.interaction.Draw({
        source: inputSource,
        type: /** @type {ol.geom.GeometryType} */ geomType,
      });
      draw_.set('name','drupalDraw');
      map.addInteraction(draw_);
      
      /*
       * geomType => check if geom wkt is deleting by drawing a new one
       * mapid => target of the wkt string (could be hidden)
       * 
       */
      draw_.on('drawstart', function(evt) {
        if(!geomType.includes("Multi")) {
          console.log("HUHU");
          console.log("wird gelöscht");
          $('#' + mapid + '-wktbox').val('');
          inputSource.clear();
        }
      });

      draw_.on('drawend', function(evt) {
        
        if(settings.parameter.target && ($('#' + mapid + '-wktbox').length > 0)) {  
          var features = inputSource.getFeatures();
          
          if (geomType.includes("Multi")) {
            console.log("Type:");
            console.log(geomType);
            
            var geom = null;
            switch (geomType) {
                  case 'MultiPoint':
                    console.log('Erzeuge MultiPoint');
                    geom = new ol.geom.MultiPoint();
                    break;
                  case 'MultiLineString':
                    console.log('Erzeuge MultiLineString');
                    geom = new ol.geom.MultiLineString();
                    break;
                  case 'MultiPolygon':
                    console.log('Erzeuge MultiPolygon');
                    geom = new ol.geom.MultiPolygon();
                    break;
            }
            
            
            var feature = evt.feature;
            switch (geomType) {
                  case 'MultiPoint':
                    geom.appendPoint(feature.getGeometry());
                    break;
                  case 'MultiLineString':
                    geom.appendLineString(feature.getGeometry());
                    break;
                  case 'MultiPolygon':
                    geom.appendPolygon(feature.getGeometry());
                    break;
            }
            
            if (features.length > 0) {
              features.forEach(function(item) {
                switch (geomType) {
                  case 'MultiPoint':
                    geom.appendPoint(item.getGeometry());
                    break;
                  case 'MultiLineString':
                    geom.appendLineString(item.getGeometry());
                    break;
                  case 'MultiPolygon':
                    geom.appendPolygon(item.getGeometry());
                    break;
                }
              });
            }
            console.log("machmut5");
            
            feature.setGeometry(geom);
            
            $('#' + mapid + '-wktbox').val(format.writeFeature(feature, {
              dataProjection: 'EPSG:4326',
              featureProjection: 'EPSG:3857'
            }));
          }
          /*
           * GeometryCollection or only one geom
           */
          else {
            features.push(evt.feature);
            $('#' + mapid + '-wktbox').val(format.writeFeatures(features, {
              dataProjection: 'EPSG:4326',
              featureProjection: 'EPSG:3857'
            }));
          }
        }
      });
    }
    
    /*
     * getting the kind of geom.
     */
    if(settings.parameter.target && ($('#' + mapid + '-wktbox').length > 0)) {
      settings.parameter.target = settings.parameter.target.replace('{uuid}',mapid);
      wkt = $('#' + mapid + '-wktbox').val();
//      console.log(map.getView().C.extent);
//      console.log(map.getView().calculateExtent());
      var geomType_ = $('#' + mapid + '-wktbox').val().toString();
      geomType = geomType_.replace(/[ (),-.0-9]/g,"");
      geomType = geomType.toUpperCase();
      if (geomType === "LINESTRING") {
        geomType = 'LineString';
      }
      if (geomType === "POLYGON") {
        geomType = 'Polygon';
      }
      if (geomType === "POINT") {
        geomType = 'Point';
      }
      if (geomType === "MULTIPOINT") {
        geomType = 'MultiPoint';
      }
      if (geomType === "MULTILINSESTRING") {
        geomType = 'MultiLinseString';
      }
      if (geomType === "MULTIPOLYGON") {
        geomType = 'MultiPolygon';
      }
      if (geomType === "GEOMETRYCOLLECTION") {
        geomType = 'GeometryCollection';
      }
      if(geomType) {
        addDraw();
      }
      
      /*
       * get the feature from textarea and add it to the map.
       */
      if (wkt !== '') {
        try {    
          var feature = format.readFeature(wkt, {
            dataProjection: 'EPSG:4326',
            featureProjection: 'EPSG:3857'
          });
          
          /*
           * check if the feature is within the bbox.
           */
          var featureBounds =  feature.getGeometry().getExtent();
          var viewBounds = ol.proj.transformExtent(map.getView().calculateExtent(), ol.proj.get('EPSG:4326'), ol.proj.get('EPSG:3857'));
          if(ol.extent.containsExtent(viewBounds, featureBounds)) {
            map.getView().fit(feature.getGeometry().getExtent(),{maxZoom:map.getView().getZoom()});
          };
          inputSource.addFeature(feature);
        } catch(e) {
          console.log("Error:");
          console.log(e)
        }
      }
    }

    /*
     * create button set.
     */
    for (var i = 0; i< types.length; i++) {
      var element = document.createElement('div');
      element.className = 'ol-unselectable ol-control';
      var button = document.createElement('img');
      button.className = "ol-control-icon " + mapid;
      var protocol = location.protocol;
      var slashes = protocol.concat("//");
      var host = slashes.concat(window.location.hostname);
      button.src = host + settings.path +"/img/" + icons[i];
      button.title = tooltips[i];
      if(types[i] === geomType) {
          button.className = "ol-control-icon" + " " + mapid + " " + 'activated';
      }
      button.setAttribute('geometry', types[i]);    
      element.appendChild(button);
      groupelement.appendChild(element);
      button.addEventListener('click', drawFunction, false);
    }

    ol.control.Control.call(this, {
      element: groupelement,
     // target: options.target
    });
  };
})(jQuery);
ol.inherits(ol.customcontrol.Draw, ol.control.Control);


