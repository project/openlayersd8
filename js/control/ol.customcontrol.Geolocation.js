ol.customcontrol.Geolocation = function(opt_options, settings, mapid, map) {  
  console.log("Geolocation: " + mapid);
  var options = opt_options || {};
  //the button
  var button = document.createElement('img');
  button.title = settings.tooltip;
  button.className = "ol-control-icon";
  var protocol = location.protocol;
  var slashes = protocol.concat("//");
  var host = slashes.concat(window.location.hostname);
  button.src = host + settings.path +"/img/" + settings.icon;
  
  var element = document.createElement('div');
  element.className = 'ol-unselectable ol-control';
  element.appendChild(button);

  var this_ = this;
  var geolocationFunction = function() {
    var geolocation = new ol.Geolocation({
      tracking: true,
      projection: map.getView().getProjection(),
    });
    var temp_view = map.getView();
    var extent = this_.getMap().getView().C.extent;

    setTimeout(function(){
      var pos = geolocation.getPosition();
      if(typeof pos !== 'undefined') {
        console.log("geolocation:");
        console.log(pos);
        if(ol.extent.containsCoordinate(extent, pos)) {
          temp_view.setCenter(pos);
        } else {
          console.log("Position is out of extent");  
        }
      } else {
        console.log('can not get geolocation');
      };
    }, 1000, geolocation, temp_view, extent);
  };

  button.addEventListener('click', geolocationFunction, false);
  button.addEventListener('touchstart', geolocationFunction, false);

  ol.control.Control.call(this, {
    element: element,
    target: options.target
  });
};
ol.inherits(ol.customcontrol.Geolocation, ol.control.Control);


