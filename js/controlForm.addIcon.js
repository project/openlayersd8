(function ($) {
  'use strict';
  console.log("JS Drupal OpenLayers ControlForm-AddIcon");
  Drupal.behaviors.addIcon = {
    attach: function (context, settings) {
      $('#openlayers-control-edit-form, #openlayers-control-add-form',context).once('controlForm').each(function () {
        $('.icon').click(function(){
            $("#edit-icon-0-value").val($(this).attr('title'));
            if(!$(this).hasClass("current")) {
                $('.icon').removeClass("current");
                $(this).addClass("current");
            }
        })
      });
    }
  }
  })(jQuery);
