(function ($) {
  'use strict';
  console.log("JS Drupal OpenLayers");
  Drupal.behaviors.openlayers = {
    attach: function (context, settings) {  
      $.each(settings.openlayers, function (mapid, data) {
        //check if div exists
        if(document.getElementById(mapid)) {
          $('#' + mapid, context).once('addMap').each(function () {
            var container = $(this);
            container.data('openlayers',new Drupal.OpenLayers($(this), mapid, data.map, data.features, data.input, data.path));
          });
        }
      });
    }
  };
  
  Drupal.OpenLayers = function(container, mapid, map_definition, features, input, path) {
    this.path = path;
    this.container = container;
    this.map_div_id = mapid;
    this.inputSource = null;
    this.inputLayer = null;
    this.map_definition = map_definition;
    
    this.settings = this.map_definition.settings;
    this.zoom = 2;
    if(this.settings.zoom) {
      this.zoom = this.settings.zoom;
    }
    this.center = [0,0];
    if(this.settings.center){
      var coords = this.settings.center.split(",");
      if(coords.length === 2) {
        this.center = [parseFloat(coords[0]),parseFloat(coords[1])];
      }
    }

    this.bounds = null;
    if(this.settings.max_extent) {
      var coords =this.settings.max_extent.split(",");
      if(coords.length === 4) {
        this.bounds = [parseFloat(coords[0]),parseFloat(coords[1]),parseFloat(coords[2]),parseFloat(coords[3])];
      }
    };
    this.layers = this.map_definition.layers;
    this.sources = this.map_definition.sources;
    this.navbar = this.map_definition.navbar;
    this.geolocation = this.settings.geolocation;
    this.featureCollection = [];
    this.overlaySource = new ol.source.Vector({});
    if (features !== null && features.length > 0) {
      var wktfeatures = this.createWKTFeatures(features);
      this.overlaySource.addFeatures(wktfeatures);
    }
    this.input = input;
    this.styles = {};
    this.olMap = null;
    this.controls = []; //als Controlbar

    this.initialise();
  }
    
  Drupal.OpenLayers.prototype.initialise = function() {
    console.log("init Map " + this.map_div_id);
    this.olMap = new ol.Map({
      layers: [],
      controls:this.controls,
      target: this.map_div_id,
      view: new ol.View({
        center: ol.proj.transform(this.center,ol.proj.get('EPSG:4326'), ol.proj.get('EPSG:3857')),
        zoom: this.zoom
      })
    });
    
    if(this.bounds) {
      this.olMap.setView(
        new ol.View({
          center: ol.proj.transform(this.center,ol.proj.get('EPSG:4326'), ol.proj.get('EPSG:3857')),
          extent: ol.proj.transformExtent(this.bounds,ol.proj.get('EPSG:4326'), ol.proj.get('EPSG:3857')),   
          zoom: this.zoom,
        })
      );
    }
    
    if(this.geolocation === 1) {
      var geolocation = new ol.Geolocation({
        tracking: true,
        projection: this.olMap.getView().getProjection(),
      });
      var temp_view = this.olMap.getView();
      var extent = ol.proj.transformExtent(this.bounds,ol.proj.get('EPSG:4326'), ol.proj.get('EPSG:3857'));
      setTimeout(function(){
        var pos = geolocation.getPosition();
        if(typeof pos !== 'undefined') {
          console.log("geolocation:");
          console.log(pos);
          //check if geolocation is in 
          if(ol.extent.containsCoordinate(extent, pos)) {
            temp_view.setCenter(pos);
          } else {
            console.log("Position is out of extent");  
          }
        } else {
            console.log('can not get geolocation');
        };
      }, 1000, geolocation, temp_view, extent);
    }; 
    
    if(Object.keys(this.sources).length >= 0 && Object.keys(this.layers).length >= 0) {
      this.addLayer();
    }
    if(this.featureCollection.length >= 0) {
      this.addFeatureOverlay();
    };

    if(this.navbar.controls.length > 0) {
      this.addOLControls(this.map_div_id);
    }
    
    document.getElementById(this.map_div_id).data = this.olMap;
  };
        
  Drupal.OpenLayers.prototype.addOLControls = function(mapid) {
    for (var id in this.navbar.controls) {
      this.navbar.controls[id].path = this.path; 
      this.olMap.addControl(new window['ol'][this.navbar.controls[id]['namespace']][this.navbar.controls[id]['machine']](null, this.navbar.controls[id], mapid, this.olMap));
    }
  }
    
  /*
   * function to add all defined layers to the map
   * @returns nothing
   */
  Drupal.OpenLayers.prototype.addLayer = function() {   
    for (var layer in this.layers) {
      this.tmp = this.layers[layer];
      switch (this.layers[layer].type) {
        case 'tile':
          this.olMap.addLayer(
            new ol.layer.Tile({
              source: this.addSource(),
              opacity: this.layers[layer].opacity,
              isBase: this.layers[layer].isBase,
              visible:this.layers[layer].isActive,
              id: this.layers[layer].id,
              title:this.layers[layer].title,
              layernames:this.layers[layer].layer,
            })
          )
          break;
        case 'image':
          this.olMap.addLayer(
            new ol.layer.Image({
              source: this.addSource(),
              opacity: this.layers[layer].opacity,
              isBase: this.layers[layer].isBase,
              visible:this.layers[layer].isActive,
              id: this.layers[layer].id,
              title:this.layers[layer].title,
              layernames:this.layers[layer].layer,
            })
          )
          break;
        case 'view':
        case 'node':
          this.olMap.addLayer(
            new ol.layer.Vector({
              source: this.addSource(this.layers[layer].features),
              opacity: this.layers[layer].opacity,
              isBase: this.layers[layer].isBase,
              visible:this.layers[layer].isActive,
              id: this.layers[layer].id,
              title:this.layers[layer].title,
              layernames:this.layers[layer].layer,
            })
          );
          break;
      }
    };
  };
    
  /*
   * function to add layer sources
   * @param {type} features
   * @returns {ol.source.XYZ|ol.source.ImageWMS|ol.source.Vector|ol.source.OSM}
   */
  Drupal.OpenLayers.prototype.addSource = function(features) {
    if(this.tmp.source !== 'none') {
      switch(this.sources[this.tmp.source].type) {
        case "osm":
          return new ol.source.OSM({wrapX: false});
          break;
        case "imagewms":
          return new ol.source.ImageWMS({
            wrapX: false,
            url: this.sources[this.tmp.source].url,
            params: {'LAYERS': this.tmp.layer},
            ratio: 1,
            serverType: this.sources[this.tmp.source].serverType,
          })
          break;
        case "xyz":
          return new ol.source.XYZ({
            url: this.sources[this.tmp.source].url,
            wrapX: false,
          });
          break;
        case "vector":
          return new ol.source.Vector({
            wrapX: false,
            features: this.createWKTFeatures(features),
          });
          break;
        }
    } else {
      return new ol.source.Vector({
        features: this.createWKTFeatures(features),
      });
    }
    return null;
  }
    
    Drupal.OpenLayers.prototype.addFeatureOverlay = function() {
        
      var featureOverlay = new ol.layer.Vector({
        source: this.overlaySource,
        id: 'fO',
        title:'FeatureOverlay',
      });
      this.olMap.addLayer(featureOverlay);
      //featureOverlay.setMap(this.olMap);
      try {
        var inputExtent = featureOverlay.getSource().getExtent();
        var viewExtent = ol.proj.transformExtent(this.bounds,ol.proj.get('EPSG:4326'), ol.proj.get('EPSG:3857'));
        if(checkInfinity(inputExtent) && ol.extent.containsExtent(viewExtent, inputExtent)){
            this.olMap.getView().fit(featureOverlay.getSource().getExtent(),{maxZoom:this.olMap.getView().getZoom()});
        } 
      } catch(ex) {
        console.log("no features set or out of map extent");  
        console.log(ex);
      }
    };
    /*
     * change every field presentation to this mod is necessary
     */
    Drupal.OpenLayers.prototype.createWKTFeatures = function(features) {
      var format = new ol.format.WKT();
      var wktfeatures = [];
      for (var i = 0; i < features.length; i++) {
        var feature = format.readFeature(features[i]['geom'], {
          dataProjection: 'EPSG:4326',
          featureProjection: 'EPSG:3857'
        });
        for (var key in features[i]) {
           feature.set(key,features[i][key]);
        }
        wktfeatures.push(feature);
      }
      return wktfeatures;
    }
})(jQuery);

function checkInfinity(extentArray) {
  for(i = 0; i < extentArray.length; i++) {
    if(!isFinite(extentArray[i])) {
      return false;         
    }
  }
  return true;    
}