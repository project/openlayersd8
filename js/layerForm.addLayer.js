(function ($) {
  'use strict';
  console.log("JS Drupal OpenLayers LayerForm-AddLayer");
  Drupal.behaviors.addLayer = {
    attach: function (context, settings) {
      $('#openlayers-layer-edit-form, #openlayers-layer-add-form',context).once('layerForm').each(function () {
        $(document).on("click", "#openlayers_clearlayerfield", function() {
          $("textarea[id^='edit-layer-machinename-0-value']").val('none');
          $( ".oltextboxcontent-disable" ).each(function() {
            $(this).removeClass("oltextboxcontent-disable");
            $(this).addClass("oltextboxcontent-enable");   
          });
        });
        
        $(document).on("click", "#openlayers_copyall", function() {
          $("textarea[id^='edit-layer-machinename-0-value']").val('none');
          $( ".oltextboxcontent-enable" ).each(function() {
            var text = $("textarea[id^='edit-layer-machinename-0-value']").val();
            if(text === 'none') {
              $("textarea[id^='edit-layer-machinename-0-value']").val($(this).attr('layer'));
            } else {
              $("textarea[id^='edit-layer-machinename-0-value']").val(text + ',' + $(this).attr('layer'));
            }
            $(this).addClass("oltextboxcontent-disable");
            $(this).removeClass("oltextboxcontent-enable");   
          });
        });
        
        $(document).on("click", ".wmslayer", function() {
          var text = $("textarea[id^='edit-layer-machinename-0-value']").val();
          if($(this).hasClass("oltextboxcontent-enable")) {
            $(this).removeClass("oltextboxcontent-enable");
            $(this).addClass("oltextboxcontent-disable");  
            if(text === 'none') {
              $("textarea[id^='edit-layer-machinename-0-value']").val($(this).attr('layer'));
            } else {
              $("textarea[id^='edit-layer-machinename-0-value']").val(text + ',' + $(this).attr('layer'));
            }
          } else {
            $(this).removeClass("oltextboxcontent-disable");
            $(this).addClass("oltextboxcontent-enable");   
            var text = $("textarea[id^='edit-layer-machinename-0-value']").val();
            text = text.replace($(this).attr('layer'),"");
            text = text.replace(",,",",");            
            if(text.slice(-1) === ","){
                text = text.substring(0,text.length - 1);
            }
            if(text.charAt(0) === ",") {
               text = text.substring(1, text.length);
            }
            if(text === "") {
                text = "none";
            }
            $("textarea[id^='edit-layer-machinename-0-value']").val(text);
          }
        });
      });
    }
  }
  })(jQuery);
